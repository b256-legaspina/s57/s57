let collection = [];

// Write the queue functions below.

function print() {
  return collection;
}

function enqueue(element) {
  collection.push(element);
  // add an element to the rear of the queue
  return collection;
}

function dequeue() {
  collection.shift();
  return collection;
  // remove an element at the front of the queue
}

function front() {
  return collection[0];
  // show the element at the front of the queue
}

function size() {
  return collection.length;
  // show the total number of elements
}

function isEmpty() {
  return collection.length === 0;
  // return a Boolean value describing whether the queue is empty or not
}

module.exports = {
  collection,
  print,
  enqueue,
  dequeue,
  front,
  size,
  isEmpty,
};
